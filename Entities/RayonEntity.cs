﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AFCEPF.AI107.Boutique.Entities
{
    public class RayonEntity
    {
        public int Id { get; set; }

        private string libelle;
        public string Libelle { 
            get 
            {
                string result = libelle.Trim();
                if (result.Length > 0)
                {
                    result = result.Substring(0, 1).ToUpper() + result.Substring(1).ToLower();
                }
                return result;
            } 
            set { this.libelle = value; } 
        }

        public RayonEntity()
        {

        }

        public RayonEntity(int id, string libelle)
        {
            Id = id;
            Libelle = libelle;
        }
    }
}
