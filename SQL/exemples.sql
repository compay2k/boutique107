/*
   Maison * -------- * Personne 1 -------- * Voiture
*/

drop database if exists boutique;

create database boutique;

use boutique;

CREATE TABLE personne (
	id int auto_increment primary key,
	nom varchar(255) not null,
	prenom varchar(255),
	date_naissance datetime default '1998-05-31'
);

CREATE TABLE voiture (
	id int auto_increment primary key,
	immatriculation varchar(255) not null,
	id_proprietaire int not null,
	CONSTRAINT FK_VOITURE_PERSONNE FOREIGN KEY (id_proprietaire) REFERENCES personne(id)
);

CREATE TABLE maison (
	id int auto_increment primary key,
	adresse varchar(255)
);

CREATE TABLE personne_maison(
	id_maison int not null,
	id_personne int not null,
	nb_parts int default 100,
	CONSTRAINT FK_PERSONNE_MAISON_PERSONNE FOREIGN KEY (id_personne) REFERENCES personne(id),
	CONSTRAINT FK_PERSONNE_MAISON_MAISON FOREIGN KEY (id_maison) REFERENCES maison(id),
	PRIMARY KEY (id_maison, id_personne)
);

