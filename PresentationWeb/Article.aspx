﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Arche.Master" AutoEventWireup="true" CodeBehind="Article.aspx.cs" Inherits="AFCEPF.AI107.Boutique.Front.Article" %>

<%@ Register Src="~/UCSousMenuCatalogue.ascx" TagPrefix="uc1" TagName="UCSousMenuCatalogue" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="principal" runat="server">

    

    <h2>
        <asp:Label ID="lblTitre" runat="server"></asp:Label>
    </h2>

    <asp:Image ID="imgPhoto" runat="server" />

    <p>
        <asp:Label ID="lblDescription" runat="server" ></asp:Label>
    </p>

    <ul>
        <li>prix : <asp:Label ID="lblPrix" runat="server" ></asp:Label> €</li>
        <li>stock : <asp:Label ID="lblStock" runat="server" ></asp:Label></li>
    </ul>

    <asp:Button ID="btnSupprimer" runat="server" OnClick="btnSupprimer_Click" Text="Supprimer" />
    
    <uc1:UCSousMenuCatalogue runat="server" ID="UCSousMenuCatalogue" />
</asp:Content>
