﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DemoRepeater.aspx.cs" Inherits="AFCEPF.AI107.Boutique.Front.DemoRepeater" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>

    <style>
        .rayon
         {
            border:2px solid red;
            margin:5px;
            padding:5px;
        }

    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Repeater ID="rptRayons" runat="server">
                <ItemTemplate>
                    <div class="rayon">
                        <header><%# DataBinder.Eval(Container.DataItem, "Libelle") %></header>            
                        <p>numero du rayon : <%# DataBinder.Eval(Container.DataItem, "Id") %></p>
                    </div>
                </ItemTemplate>
            </asp:Repeater>
        </div>
    </form>
</body>
</html>
