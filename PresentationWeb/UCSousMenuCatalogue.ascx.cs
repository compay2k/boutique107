﻿using AFCEPF.AI107.Boutique.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AFCEPF.AI107.Boutique.Front
{
    public partial class UCSousMenuCatalogue : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CatalogueBU bu = new CatalogueBU();
                ddlArticles.DataSource = bu.GetListeArticles();
                ddlArticles.DataTextField = "Nom";
                ddlArticles.DataValueField = "Id";
                ddlArticles.DataBind();
            }
        }

        protected void btnDetails_Click(object sender, EventArgs e)
        {
            String idArticle = ddlArticles.SelectedValue;
            Response.Redirect("Article.aspx?id=" + idArticle);
        }
    }
}