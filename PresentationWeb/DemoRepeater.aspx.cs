﻿using AFCEPF.AI107.Boutique.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AFCEPF.AI107.Boutique.Front
{
    public partial class DemoRepeater : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            CatalogueBU bu = new CatalogueBU();
            rptRayons.DataSource = bu.GetListeRayons();
            rptRayons.DataBind();
        }
    }
}