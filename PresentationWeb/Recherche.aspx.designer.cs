﻿//------------------------------------------------------------------------------
// <généré automatiquement>
//     Ce code a été généré par un outil.
//
//     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
//     le code est régénéré.
// </généré automatiquement>
//------------------------------------------------------------------------------

namespace AFCEPF.AI107.Boutique.Front
{


    public partial class Recherche
    {

        /// <summary>
        /// Contrôle txtNom.
        /// </summary>
        /// <remarks>
        /// Champ généré automatiquement.
        /// Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txtNom;

        /// <summary>
        /// Contrôle ddlRayon.
        /// </summary>
        /// <remarks>
        /// Champ généré automatiquement.
        /// Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList ddlRayon;

        /// <summary>
        /// Contrôle txtPrixMax.
        /// </summary>
        /// <remarks>
        /// Champ généré automatiquement.
        /// Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txtPrixMax;

        /// <summary>
        /// Contrôle btnRechercher.
        /// </summary>
        /// <remarks>
        /// Champ généré automatiquement.
        /// Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button btnRechercher;

        /// <summary>
        /// Contrôle gvArticles.
        /// </summary>
        /// <remarks>
        /// Champ généré automatiquement.
        /// Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.GridView gvArticles;
    }
}
