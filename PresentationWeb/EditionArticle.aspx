﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Arche.Master" AutoEventWireup="true" CodeBehind="EditionArticle.aspx.cs" Inherits="AFCEPF.AI107.Boutique.Front.EditionArticle" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="principal" runat="server">

    Nom : <asp:TextBox ID="txtNom" runat="server"></asp:TextBox>
    <br />
    Description : <asp:TextBox ID="txtDescription" TextMode="MultiLine" runat="server"></asp:TextBox>
    <br />
    Prix unitaire : <asp:TextBox ID="txtPrix" runat="server"></asp:TextBox>
    <br />
    Stock : <asp:TextBox ID="txtStock" runat="server"></asp:TextBox>
    <br />
    Rayon : <asp:DropDownList ID="ddlRayon" runat="server"></asp:DropDownList>
    <br />
    
    <asp:Button ID="btnEnregistrer" runat="server" OnClick="btnEnregistrer_Click" Text="Mettre à jour" />
    <asp:Label ID="lblMessage" Visible="false" CssClass="message" runat="server" Text=""></asp:Label>

</asp:Content>
