﻿using AFCEPF.AI107.Boutique.Business;
using AFCEPF.AI107.Boutique.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AFCEPF.AI107.Boutique.Front
{
    public partial class Recherche : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CatalogueBU bu = new CatalogueBU();
                gvArticles.DataSource = bu.GetArticleDetails();
                gvArticles.DataBind();

                List<RayonEntity> rayons = bu.GetListeRayons();
                rayons.Insert(0, new RayonEntity(-1, "Tous les rayons"));
                ddlRayon.DataSource = rayons;
                ddlRayon.DataTextField = "Libelle";
                ddlRayon.DataValueField = "Id";
                ddlRayon.DataBind();
            }

        }

        protected void btnRechercher_Click(object sender, EventArgs e)
        {
            CatalogueBU bu = new CatalogueBU();
            string nom = txtNom.Text;
            int idRayon = int.Parse(ddlRayon.SelectedValue);
            // le prix max est null par défaut :
            float? prixMax = null;
            // si valeur saisie, je la récupère :
            if (! String.IsNullOrEmpty(txtPrixMax.Text))
            {
                prixMax = float.Parse(txtPrixMax.Text);
            }
            gvArticles.DataSource = bu.RechercherArticles(nom, idRayon, prixMax);
            gvArticles.DataBind();
        }

        protected void ddlRayon_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}